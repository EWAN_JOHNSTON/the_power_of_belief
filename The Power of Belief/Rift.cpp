#include "Rift.h"

Rift::Rift(sf::Texture& riftTexture, sf::Vector2u newScreenSize)
	: sprite(riftTexture)
	, screenSize(newScreenSize)
	, velocity(500)
	, alive(true)
	, timer(2.0f)
{
	Reset(screenSize);
}

void Rift::Update(sf::Time frameTime)
{
	sf::Vector2f oldPosition = sprite.getPosition();
	sf::Vector2f newPosition;
	newPosition = sf::Vector2f(oldPosition.x - velocity * frameTime.asSeconds(), sprite.getPosition().y);
	sprite.setPosition(newPosition);
}

void Rift::Reset(sf::Vector2u screenSize)
{
	sprite.setPosition(10 , screenSize.y/2 -sprite.getTexture()->getSize().y / 2);
	alive = true;
	timer = 2.0f;
}

void Rift::Draw(sf::RenderWindow& gameWindow)
{
	gameWindow.draw(sprite);
}

void Rift::ReduceTimer(float modifier)
{
	timer -= modifier;
}

bool Rift::GetAlive()
{
	return alive;
}

float Rift::GetTimer()
{
	return timer;
}

sf::Vector2f Rift::GetPosition()
{
	return sprite.getPosition();
}

void Rift::SetAlive(bool newAlive)
{
	alive = newAlive;
}

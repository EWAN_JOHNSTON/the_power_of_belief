#pragma once
#include <SFML/Graphics.hpp>//Libary needed for using sprites, graphics and fonts
#include <SFML/Audio.hpp>//Libary needed for playing any form of sound
#include <vector>//Libary for handling Collections of objects
class Boss
{
public:
	Boss(sf::Texture& bossTexture, sf::Vector2u newScreenSize);

	void Update(sf::Time frameTime);
	void Reset(sf::Vector2u screenSize);
	void Draw(sf::RenderWindow& gameWindow);

	//getters
	bool GetAlive();
	int GetHealth();
	sf::FloatRect GetHitbox();
	//setters
	void SetAlive(bool newAlive);
	void SetHealth(int newHealth);

private:
	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
	sf::Vector2u screenSize;
	int health;
	bool isBAlive;
};


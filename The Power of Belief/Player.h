#pragma once
#include <SFML/Graphics.hpp>//Libary needed for using sprites, graphics and fonts
#include <SFML/Audio.hpp>//Libary needed for playing any form of sound
#include <vector>//Libary for handling Collections of objects
#include "Bullet.h"
class Player
{
public:
	// Constructor
	Player(sf::Texture& playerTexture, sf::Vector2u newScreenSize,
		std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer);
	void Input();
	void Update(sf::Time frameTime);
	void Reset(sf::Vector2u screenSize);
	void Draw(sf::RenderWindow& gameWindow);
	void ReduceTimer(float modifier);

	//getters
	bool GetAlive();
	sf::FloatRect GetHitbox();
	int GetLives();
	float GetTimer();


	//setters
	void SetAlive(bool newAlive);
	void SetDamage(int damage);
	void SetTimer(float time);


private:

	// Variables (data members) used by this class
	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
	sf::Vector2u screenSize;
	std::vector<Bullet>& bullets;
	sf::Texture& bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletFireSound;
	int lives;
	bool alive;
	float damageTimer;
};
#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
class Rift
{
public:
	Rift(sf::Texture& riftTexture, sf::Vector2u newScreenSize);
	void Update(sf::Time frameTime);
	void Reset(sf::Vector2u screenSize);
	void Draw(sf::RenderWindow& gameWindow);
	void ReduceTimer(float modifier);

	bool GetAlive();
	float GetTimer();
	sf::Vector2f GetPosition();

	void SetAlive(bool newAlive);
private:
	sf::Sprite sprite;
	float velocity;
	sf::Vector2u screenSize;
	bool alive;
	float timer;
};


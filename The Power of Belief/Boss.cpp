#include "Boss.h"


Boss::Boss(sf::Texture& bossTexture, sf::Vector2u newScreenSize)
	: sprite(bossTexture)
	, velocity(0.0f,0.0f)
	, speed(350.0f)
	, screenSize(newScreenSize)
	, health(50)
	, isBAlive(true)
{
	srand(time(0));
	Reset(screenSize);
	velocity.y = speed;
}

void Boss::Update(sf::Time frameTime)
{
	sf::Vector2f oldPosition = sprite.getPosition();
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();


	if (newPosition.y <= 0 || newPosition.y >= screenSize.y - sprite.getTexture()->getSize().y)
	{
		int pickSpeed = (rand() % 600) + 200;
		if (speed < 0)
		{
			speed = -1 * pickSpeed;
		}
		if (speed > 0)
		{
			speed = pickSpeed;
		}

		newPosition = oldPosition;
		speed = -speed; //invert the speed	
		velocity.y = speed;
	}

	sprite.setPosition(newPosition);
}

void Boss::Reset(sf::Vector2u screenSize)
{
	sprite.setPosition(
		(9 * screenSize.x) / 10 - sprite.getTexture()->getSize().x / 2,
		screenSize.y / 2 - sprite.getTexture()->getSize().y / 2);
	isBAlive = true;
	health = 50;
}

void Boss::Draw(sf::RenderWindow& gameWindow)
{
	gameWindow.draw(sprite);
}

bool Boss::GetAlive()
{
	return isBAlive;
}

int Boss::GetHealth()
{
	return health;
}

sf::FloatRect Boss::GetHitbox()
{	
	return sprite.getGlobalBounds();
}

void Boss::SetAlive(bool newAlive)
{
	isBAlive = newAlive;
}

void Boss::SetHealth(int newHealth)
{

	health += newHealth;
}

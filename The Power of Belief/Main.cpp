#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <vector>
#include "Player.h"
#include "Boss.h"
#include "Bullet.h"
#include "Star.h"
#include "Enemy.h"
#include "Rift.h"


int main()
{
	// Declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// Set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "The Power of Belief", sf::Style::Titlebar | sf::Style::Close);

	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------

	// Game Clock
	// Create a clock to track time passed each frame in the game
	sf::Clock gameClock;

	//I know that we we were taught a better way of level design to make items from classes more restirct
	//but this year was not great for my learning, so I am focusing on a working game more than a secure one.


	//font
	sf::Font gameFont;
	gameFont.loadFromFile("Assets/Font/mainFont.ttf");
	

	//TEXTS
	
	//game over text
	sf::Text gameOverText;
	gameOverText.setFont(gameFont);
	gameOverText.setString("GAME OVER\n	TRY AGAIN\n\nPress R to restart\nor Q to quit");
	gameOverText.setCharacterSize(72);
	gameOverText.setFillColor(sf::Color::Red);
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);
	
	//score
	float score = 0.0f;
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(50);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(0 , 60);

	//game win text
	sf::Text gameWinText;
	gameWinText.setFont(gameFont);
	gameWinText.setString("");
	gameWinText.setCharacterSize(72);
	gameWinText.setFillColor(sf::Color::Cyan);
	gameWinText.setStyle(sf::Text::Bold | sf::Text::Italic);
	gameWinText.setPosition(gameWindow.getSize().x / 2 - gameWinText.getLocalBounds().width / 2, 150);

	//lives
	int lives = 0;
	sf::Text livesText;
	livesText.setFont(gameFont);
	livesText.setString("Lives: 0");
	livesText.setCharacterSize(50);
	livesText.setFillColor(sf::Color::White);
	livesText.setPosition(0, 110);

	sf::Text healthText;
	healthText.setFont(gameFont);
	healthText.setString("Health: 0");
	healthText.setCharacterSize(50);
	healthText.setFillColor(sf::Color::White);
	healthText.setPosition(gameWindow.getSize().x - scoreText.getLocalBounds().width - 100, 60);


	/*sf::Text timeText;
	timeText.setFont(gameFont);
	timeText.setString("Timer: 0");
	timeText.setCharacterSize(50);
	timeText.setFillColor(sf::Color::White);
	timeText.setPosition(gameWindow.getSize().x - scoreText.getLocalBounds().width - 100, gameWindow.getSize().y - scoreText.getLocalBounds().height - 100);
	*/

	//OBJECTS

	//bullets
	std::vector<Bullet> playerBullets;
	sf::Texture playerBulletTexture;
	playerBulletTexture.loadFromFile("Assets/Graphics/playerBullet.png");
	sf::SoundBuffer playerBulletSound;
	playerBulletSound.loadFromFile("Assets/Audio/Fire.ogg");

	//stars
	sf::Texture starTexture;
	starTexture.loadFromFile("Assets/Graphics/star.png");
	std::vector<Star>stars;
	int numOfStars = 7;
	for (int i = 0; i < numOfStars; ++i)
	{
		stars.push_back(Star(starTexture, gameWindow.getSize()));
	}

	//Enemy Wall
	sf::Texture wallTexture;
	wallTexture.loadFromFile("Assets/Graphics/EnergyBall.png");
	std::vector<Enemy> wall;
	int numOfBalls = 8;
	int hole = (rand() % 8); //create a randomiser to not display one of the members of wall (-1 makes it in range)
	for (int i = 0; i < numOfBalls; ++i)
	{
		wall.push_back(Enemy(wallTexture, gameWindow.getSize()));
		sf::Vector2f newPos;
		newPos.x =(8 * gameWindow.getSize().x) / 10;
		newPos.y += wallTexture.getSize().y*i;
		wall[i].SetPosition(newPos);
		if (i == hole)
			wall[i].SetHole(true);
	}
	
	
	//Player Setup
	sf::Texture playerTexture;
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	Player playerInstance(playerTexture,
		gameWindow.getSize(),
		playerBullets, playerBulletTexture, playerBulletSound);

	//boss setup
	sf::Texture bossTexture;
	bossTexture.loadFromFile("Assets/Graphics/boss.png");
	Boss bossInstance(bossTexture, gameWindow.getSize());

	//rift set up (3D object)
	sf::Texture riftTexture;
	riftTexture.loadFromFile("Assets/Graphics/Rift.png");
	Rift riftInstance(riftTexture, gameWindow.getSize());

	//game states
	bool winState = false;
	bool loseState = false;


	//AUDIO

	//music
	sf::Music gameMusic;
	gameMusic.openFromFile("Assets/Audio/electroMusic.ogg");
	gameMusic.setVolume(25);
	gameMusic.play();

	//player hit sound
	sf::SoundBuffer PHBuffer;
	PHBuffer.loadFromFile("Assets/Audio/PlayerHit.ogg");
	sf::Sound playerHitSFX(PHBuffer);
	playerHitSFX.setVolume(30);

	//boss hit sound
	sf::SoundBuffer BHBuffer;
	BHBuffer.loadFromFile("Assets/Audio/BossHit.ogg");
	sf::Sound bossHitSFX(BHBuffer);
	bossHitSFX.setVolume(30);

	//wall spawn sound 
	sf::SoundBuffer wallBuffer;
	wallBuffer.loadFromFile("Assets/Audio/explosion.ogg");
	sf::Sound wallSFX(wallBuffer);
	wallSFX.setVolume(30);
	bool wallPlayed = false;

	//bullet on wall sound
	sf::SoundBuffer BWBuffer;
	BWBuffer.loadFromFile("Assets/Audio/BulletBurn.ogg");
	sf::Sound BWBurnSFX(BWBuffer);
	BWBurnSFX.setVolume(30);

	//win state sound 
	sf::SoundBuffer winBuffer;
	winBuffer.loadFromFile("Assets/Audio/win.ogg");
	sf::Sound winSFX(winBuffer);
	winSFX.setVolume(30);
	bool winPlayed = false;
	//lose state sound
	sf::SoundBuffer loseBuffer;
	loseBuffer.loadFromFile("Assets/Audio/loss.ogg");
	sf::Sound loseSFX(loseBuffer);
	loseSFX.setVolume(30);
	bool losePlayed = false;


	while (gameWindow.isOpen())
    {


        sf::Event event;
        while (gameWindow.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                gameWindow.close();
        }
		if (bossInstance.GetAlive() == false)
		{
			winState = true;
		}
		//INPUT SECTION ---------
		playerInstance.Input();


		// Close window on Escape pressed.
		/*if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			gameWindow.close();
		}
		*/

		if (loseState || winState)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
			{
				gameWindow.close();
			}
			if ((sf::Keyboard::isKeyPressed(sf::Keyboard::R)))
			{
				loseState = false;
				winState = false;
				losePlayed = false;
				winPlayed = false;
				score = 0;
				gameMusic.play();
				playerInstance.Reset(gameWindow.getSize());
				bossInstance.Reset(gameWindow.getSize());
				for (int i = 0; i < wall.size(); ++i) wall[i].Reset(gameWindow.getSize());
				riftInstance.Reset(gameWindow.getSize());
				playerBullets.clear();
			}
		}
		//UPDATE SECTION----------
		// Get the time passed since the last frame and restart our game clock
		sf::Time frameTime = gameClock.restart();
		int tempDamage = 0; //start of damage modifier
		if (!loseState && !winState)
		{
			gameMusic.setLoop(true);
			scoreText.setString("Score: " + std::to_string((int)score));//score update
			healthText.setString("Health: " + std::to_string((int)bossInstance.GetHealth()));//health update
			livesText.setString("Lives: " + std::to_string((int)playerInstance.GetLives()));//lives update
			//timeText.setString("Time: " + std::to_string((float)riftInstance.GetTimer()));//timer update

			playerInstance.Update(frameTime);
			playerInstance.ReduceTimer(frameTime.asSeconds());
			if (riftInstance.GetTimer() > 0.0f)riftInstance.ReduceTimer(frameTime.asSeconds());
			if (riftInstance.GetTimer() <= 0.0f) riftInstance.Update(frameTime);
			if (riftInstance.GetPosition().x + wallTexture.getSize().x < 0) riftInstance.SetAlive(false);
			bossInstance.Update(frameTime);


			//move the wall
			int hole = (rand() % 7); //create a randomiser to not display one of the members of wall

			for (int i = 0; i < wall.size(); ++i)
			{

				if (wall[i].GetTimer() > 0.0f)
				{
					wall[i].ReduceTimer(frameTime.asSeconds());
				}
				if (wall[i].GetTimer() <= 0.0f)
				{
					wall[i].Update(frameTime);
					wall[i].SetAlive(true);

				}
				//if it is offscreen
				if (wall[i].GetPosition().x + wallTexture.getSize().x < 0)
				{
					wall[i].SetHole(false);
					//wall[i].SetAlive(true);
					wall[i].Reset(gameWindow.getSize());
					wallPlayed = false;
					if (i == hole)
						wall[i].SetHole(true);
				}

				//check for collision agaisnt player
				sf::FloatRect wallBounds = wall[i].GetHitbox();
				sf::FloatRect playerBounds = playerInstance.GetHitbox();
				if (wallBounds.intersects(playerBounds) && !wall[i].GetHole())
				{
					if (playerInstance.GetTimer() <= 0)
					{
						playerHitSFX.play();
						playerInstance.SetDamage(1);
						playerInstance.SetTimer(2.0f);
						score -= 10;
					}
					wall[i].SetHole(true);
					if (playerInstance.GetLives() == 0)
					{
						playerInstance.SetAlive(false);
						loseState = true;
					}
				}

			}

			if (wall[0].GetTimer() <= 0 && wallPlayed == false)
			{
				wallSFX.play();
				wallPlayed = true;
			}

			//update the player bullets
			for (int i = 0; i < playerBullets.size(); ++i)
			{
				playerBullets[i].Update(frameTime);

				//check for collision agaisnt boss
				sf::FloatRect bulletBounds = playerBullets[i].GetHitbox();
				sf::FloatRect bossBounds = bossInstance.GetHitbox();
				if (bulletBounds.intersects(bossBounds))
				{
					bossHitSFX.play();
					playerBullets[i].SetAlive(false);
					tempDamage -= 1;
					score += 100;
				}
				//check for collision with wall
				for (int j = 0; j < wall.size(); ++j)
				{
					sf::FloatRect wallBounds = wall[j].GetHitbox();
					if (bulletBounds.intersects(wallBounds) && wall[j].GetAlive() && !wall[j].GetHole())
					{
						playerBullets[i].SetAlive(false);
						BWBurnSFX.play();
					}
				}
			}


			// Update the stars
			for (int i = 0; i < stars.size(); ++i)
			{
				stars[i].Update(frameTime);
			}

			bossInstance.SetHealth(tempDamage);
			if (bossInstance.GetHealth() == 0)
			{
				bossInstance.SetAlive(false); //if the boss is dead, set him to be dead
				score += 1000;
			}

			score -= (float)frameTime.asSeconds();
			if (score < 0) score = 0;

		}
		//DRAW SECTION ----------
        gameWindow.clear();


		for (int i = 0; i < stars.size(); ++i)
		{
			stars[i].DrawTo(gameWindow);
		}
		if (!loseState && !winState)
		{
			//draw the player bullets
			for (int i = 0; i < playerBullets.size(); ++i)
			{
				playerBullets[i].DrawTo(gameWindow);
				// If the bullet is dead, delete it
				if (!playerBullets[i].GetAlive())
				{
					// Remove the item from the vector
					playerBullets.erase(playerBullets.begin() + i);
				}

			}

			if (bossInstance.GetAlive()) bossInstance.Draw(gameWindow);

			for (int i = 0; i < wall.size(); ++i)
			{
				if (wall[i].GetAlive() && !wall[i].GetHole())
				{
					wall[i].Draw(gameWindow);
				}
			}

			if (playerInstance.GetAlive()) playerInstance.Draw(gameWindow);
			if (riftInstance.GetAlive()) riftInstance.Draw(gameWindow);

			gameWindow.draw(scoreText);
			gameWindow.draw(healthText);
			gameWindow.draw(livesText);
			//gameWindow.draw(timeText);
		}
		if (loseState)
		{
			gameWindow.draw(gameOverText); 
			if (!losePlayed) // stop the sfx constantly playing
			{
				gameMusic.setLoop(false);
				gameMusic.stop();
				loseSFX.play();
				losePlayed = true;
			}
			gameWindow.draw(healthText);
			gameWindow.draw(livesText);
		}
		if (winState)
		{
			gameWinText.setString("GAME WIN\nSCORE: " + std::to_string((int)score) + " \n\nPress R to restart\nor Q to quit");
			gameWindow.draw(gameWinText);
			if (!winPlayed)// stop the sfx constantly playing
			{
				gameMusic.setLoop(false);
				gameMusic.stop();
				winSFX.play();
				winPlayed = true;
			}
			gameWindow.draw(livesText);
		}
        gameWindow.display();
    }

    return 0;
}
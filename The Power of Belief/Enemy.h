#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
class Enemy
{
public:
	Enemy(sf::Texture& enemyTexture, sf::Vector2u newScreenSize);
	void Update(sf::Time frameTime);
	void Reset(sf::Vector2u screenSize);
	void Draw(sf::RenderWindow& gameWindow);
	void ReduceTimer(float modifier);

	bool GetAlive();
	sf::Vector2f GetPosition();
	sf::FloatRect GetHitbox();
	float GetTimer();
	bool GetHole();

	void SetAlive(bool newAlive);
	void SetTimer(int newTimer); //Not used in game, but can be used if game wanted to change difficulty
	void SetPosition(sf::Vector2f newPosition);
	void SetHole(bool newHole);

private:
	sf::Sprite sprite;
	float velocity;
	sf::Vector2u screenSize;
	bool alive;
	float timer;
	bool isHole;
};


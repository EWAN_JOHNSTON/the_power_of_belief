#include "Enemy.h"

Enemy::Enemy(sf::Texture& enemyTexture, sf::Vector2u newScreenSize)
	: sprite(enemyTexture)
	, screenSize(newScreenSize)
	, velocity(250.0f)
	, alive(false)
	, timer(5.0f)
	, isHole(false)
{
	Reset(screenSize);
}

void Enemy::Update(sf::Time frameTime)
{
	sf::Vector2f oldPosition = sprite.getPosition();
	sf::Vector2f newPosition;
	newPosition = sf::Vector2f(oldPosition.x - velocity * frameTime.asSeconds(), sprite.getPosition().y);
	sprite.setPosition(newPosition);
}


void Enemy::Reset(sf::Vector2u screenSize)
{
	sprite.setPosition((8 * screenSize.x) / 10, sprite.getPosition().y);
	alive = false;
	timer = 5.0f;
}

void Enemy::Draw(sf::RenderWindow& gameWindow)
{
	gameWindow.draw(sprite);
}

void Enemy::ReduceTimer(float modifier)
{
	timer -= modifier;
}

bool Enemy::GetAlive()
{
	return alive;
}

sf::FloatRect Enemy::GetHitbox()
{
	return sprite.getGlobalBounds();
}

sf::Vector2f Enemy::GetPosition()
{
	return sprite.getPosition();
}

void Enemy::SetAlive(bool newAlive)
{ 
	alive = newAlive;
}

void Enemy::SetTimer(int newTimer)
{
	timer = newTimer;
}

float Enemy::GetTimer()
{
	return timer;
}

bool Enemy::GetHole()
{
	return isHole;
}

void Enemy::SetHole(bool newHole)
{
	isHole = newHole;
}

void Enemy::SetPosition(sf::Vector2f newPosition)
{
	sprite.setPosition(newPosition);
}

